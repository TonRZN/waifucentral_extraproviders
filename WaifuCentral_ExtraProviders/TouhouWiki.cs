﻿using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using HTML_Scrapper.Core;
using HTML_Scrapper.Helper;
using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using WaifuCentral_Lib.Services;
using WaifuCentral_Lib.Structure;

namespace WaifuCentral_Extra
{
    [Provider("TouhouWiki", Description = "Touhou Wiki Provider")]
    public class TouhouWiki : IProvider
    {
        string CharacterQueryURL { get => @"https://en.touhouwiki.net/wiki/Characters"; }
        string CharacterBaseURL { get => @"https://en.touhouwiki.net"; }
        string CharacterPicturesBaseURL { get => @"https://en.touhouwiki.net/images/"; }
        string CharacterPicturesRemovalURL { get => @"/images/thumb/(.*)/.*"; }

        public HtmlDocument SearchPage(string name)
        {   
            try
            {
                return HTMLScrapper.GetDocumentFromURL(name);
            }
            catch (Exception e)
            {
                //Logger.Error(e.ToString());
                return null;
            }
        }

        public string GetAlternativeTitles(object html) => HTMLScrapper.GetElementContent((html as HtmlDocument), "/html[1]/body[1]/div[3]/div[3]/div[4]/div[1]/div[1]/table[1]/tbody[1]/tr[2]/td[1]/span[1]/span[1]/span[1]").DeEntitize();

        public string GetDescription(object html) => HTMLScrapper.GetElementContent((html as HtmlDocument), "/html[1]/body[1]/div[3]/div[3]/div[4]/div[1]/div[1]/p[1]").DeEntitize();

        public string GetImage(object html) => $"{CharacterPicturesBaseURL}{Regex.Replace(HTMLScrapper.GetElementAttribute((html as HtmlDocument), "//table[@class='infobox vcard outcell']//img", "src"), CharacterPicturesRemovalURL, "$1")}";
        
        public JArray GetImages(object html)
        {
            JArray result = new JArray();

            for(int i = 1; i < 6; i++)
            {
                for(int j = 1; i < 100; i++)
                {
                    String tmp = HTMLScrapper.GetElementAttribute((html as HtmlDocument), $"//ul[{i}]//li[{j}]//div[1]//div[1]//div[1]//a[1]//img[1]", "src");

                    if (!String.IsNullOrEmpty(tmp))
                        result.Add($"{CharacterPicturesBaseURL}{Regex.Replace(tmp, CharacterPicturesRemovalURL, "$1")}");
                    else
                        j = 101;
                }
            }

            return result;
        }

        public string GetName(object html) => HTMLScrapper.GetElementContent((html as HtmlDocument), "//h1[@id='firstHeading']").DeEntitize();

        public string GetNativeName(object html) => HTMLScrapper.GetElementContent((html as HtmlDocument), "//th[@class='vcard; incell_top']//b//span").DeEntitize();

        public string GetSeries(object param) => "Touhou Project";

        public string GetSource(object name) => $"{CharacterBaseURL}{HTMLScrapper.GetElementAttribute(HTMLScrapper.GetDocumentFromURL($"{CharacterQueryURL}", Encoding.UTF8), $"//table[@class='outcell']//a[contains(text(),'{CultureInfo.CurrentCulture.TextInfo.ToTitleCase((name as string).ToLower())}')]", "href")}";

        public Waifu Search(string name)
        {
            Waifu result = new Waifu();

            result.Source = GetSource(name);

            HtmlDocument html = SearchPage(result.Source);

            result.Name = GetName(html);
            result.NativeName = GetNativeName(html);

            result.AlternativeTitles = GetAlternativeTitles(html);

            result.Description = GetDescription(html);
            result.Series = "Touhou Project";

            JsonMergeSettings jsonMerge = new JsonMergeSettings { MergeArrayHandling = MergeArrayHandling.Union };
            result.Images.Merge(GetImage(html), jsonMerge);
            result.Images.Merge(GetImages(html), jsonMerge);

            return result;
        }
    }
}
